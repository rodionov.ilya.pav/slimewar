using UnityEngine;
using Player;
[CreateAssetMenu(fileName = "New Effect", menuName = "Effects/Effect_BoostSpeedMovement", order = 51)]
public class Effect_BoostSpeedMovement : Effect
{
    public override void Execute(PlayerController player)
    {
        player.SpeedUpMovement(Power);
    }
}