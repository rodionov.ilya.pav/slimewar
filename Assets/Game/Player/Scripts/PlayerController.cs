using UnityEngine;
namespace Player
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(PlayerControllerCamera))]
    [RequireComponent(typeof(PlayerControllerAttack))]
    [RequireComponent(typeof(PlayerControllerEffects))]
    [RequireComponent(typeof(PlayerControllerMovement))]

    public class PlayerController : MonoBehaviour
    {
        public bool IsKey = false;
        public GameController _gameController;
        [SerializeField] public Transform ShootTransform;
        private bool Dead = false;
        [Space]
        private PlayerControllerMovement playerMovement;
        private PlayerControllerEffects playerEffects;
        private PlayerControllerAttack playerAttack;
        [Space]
        [SerializeField] public AudioClip Jump_AudioClip;
        [SerializeField] public AudioClip BoostJump_AudioClip;
        [SerializeField] private AudioClip Damage_AudioClip;
        [SerializeField] public AudioClip Shoot_AudioClip;
        public GameController gameController
        {
            get
            {
                if (_gameController == null) _gameController = FindObjectOfType<GameController>();
                return _gameController;
            }
        }
        private void OnEnable()
        {
            playerMovement = GetComponent<PlayerControllerMovement>();
            playerEffects = GetComponent<PlayerControllerEffects>();
            playerAttack = GetComponent<PlayerControllerAttack>();
        }
        public float GetSpeedMovement()
        {
            return playerMovement.speed;
        }
        public void SpeedUpMovement(float speed)
        {
            playerMovement.speed += speed;
        }
        public void PowerUpJump(float powerUP)
        {
            playerMovement.addForce += powerUP;
        }
        public void AddEffectToStorage(Effect effect)
        {
            playerEffects.AddEffectToStorage(effect);
        }
        public void ClearEffectToStorage()
        {
            playerEffects.ClearEffectToStorage();
        }
        public void TakeDamage()
        {
            gameController.PlayOneShotAndDestroy(Damage_AudioClip, transform);
            gameController.Warning_ShowAndRestart(this);
            Dead = true;
        }
        private void OnDestroy()
        {
            if (!Dead) return;
            ClearEffectToStorage();
        }
        public void FinishLevel()
        {
            playerMovement.animator.SetBool("move", false);
            playerMovement.animator.SetTrigger("win");
            StoppingPlayer();
        }
        private void StoppingPlayer()
        {
            ClearEffectToStorage();

            playerMovement.enabled = false;
            playerEffects.enabled = false;
            playerAttack.enabled = false;
        }
    }
}