using UnityEngine;
using Player;

public class Effect : ScriptableObject
{
    [SerializeField] public Sprite icon;
    [SerializeField] public float Power = 1.5f; 
    public virtual void Execute(PlayerController player)
    {
    }
}