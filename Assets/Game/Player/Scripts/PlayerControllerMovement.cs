using UnityEngine;
namespace Player
{
    public class PlayerControllerMovement : MonoBehaviour
    {
        public float speed = 150;
        public float addForce = 7;
        [SerializeField] private KeyCode addForceButton = KeyCode.Space;
        [SerializeField] private KeyCode RightMove = KeyCode.D;
        [SerializeField] private KeyCode LeftMove = KeyCode.A;

        public bool isFacingRight = true;

        private Vector3 direction;
        private float horizontal;
        private bool jump;

        public Animator animator;
        public Rigidbody2D body;

        private PlayerController playerController;
        private void OnEnable()
        {
            playerController = GetComponent<PlayerController>();
            animator = GetComponent<Animator>();
            body = GetComponent<Rigidbody2D>();
        }

        void OnCollisionStay2D(Collision2D coll)
        {
            if (coll.transform.tag == "Ground")
            {
                body.drag = 10;
                jump = true;
            }
        }

        void OnCollisionExit2D(Collision2D coll)
        {
            if (coll.transform.tag == "Ground")
            {
                body.drag = 0;
                jump = false;
            }
        }

        void FixedUpdate()
        {
            body.AddForce(direction * body.mass * speed);

            if (Mathf.Abs(body.velocity.x) > speed / 100f)
            {
                body.velocity = new Vector2(Mathf.Sign(body.velocity.x) * speed / 100f, body.velocity.y);
            }
            if (Input.GetKey(addForceButton) && jump)
            {
                GetComponent<AudioSource>().clip = playerController.Jump_AudioClip;
                GetComponent<AudioSource>().Play();
                body.velocity = new Vector2(0, addForce);
            }
        }

        void Flip()
        {
            isFacingRight = !isFacingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        void Update()
        {
            horizontal = Input.GetAxis("Horizontal");

            direction = new Vector2(horizontal, 0);

            if (horizontal > 0 && !isFacingRight) Flip(); else if (horizontal < 0 && isFacingRight) Flip();

            if (Input.GetKey(RightMove) || Input.GetKey(LeftMove)) animator.SetBool("move", true);
            else animator.SetBool("move", false);
        }
    }
}