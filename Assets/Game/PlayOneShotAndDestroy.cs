using UnityEngine;

public class PlayOneShotAndDestroy : MonoBehaviour
{
    public void Play(AudioClip clip)
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.Play();
        Destroy(gameObject, clip.length);
    }
}