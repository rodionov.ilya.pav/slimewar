using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private AudioClip switchingButton_AudioClip;
    [SerializeField] private AudioClip chooseButton_AudioClip;
    private AudioSource audioSource;
    private void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void SwitchingButton()
    {
        audioSource.PlayOneShot(switchingButton_AudioClip);
    }
    public void ChooseButton()
    {
        audioSource.PlayOneShot(chooseButton_AudioClip);
    }
    public void BeginGame()
    {
        SceneManager.LoadScene("Level-1");
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}