using Player;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] private string LevelName = "Level-2";
    private GameController gameController;
    private void OnEnable()
    {
        gameController = FindObjectOfType<GameController>();
        if (gameController == null)
        {
            Debug.LogError("GameController Not Found!");
            enabled = false;
            return;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerController player = collision.GetComponent<PlayerController>();
            if (player != null && player.IsKey)
            {
                gameController.FinishLevel(LevelName);
            }
        }
    }
}