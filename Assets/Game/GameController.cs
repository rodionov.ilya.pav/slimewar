using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using Player;
using TMPro;

public class GameController : MonoBehaviour
{
    [SerializeField] private float TimeReload = 10;
    private float Timer = 0;
    private bool Pause = false;
    [SerializeField] private PlayerController playerController;
    [SerializeField] private MusicController music_gameObject;
    [Space]
    [SerializeField] private TMP_Text WarningResetLevel_Text;
    [SerializeField] private TMP_Text WarningOpenDoor_Text;
    [SerializeField] private TMP_Text Timer_Text;
    [SerializeField] public Image PanelEffects;
    [Space]
    [SerializeField] private AudioClip FinishLevel_AudioClip;
    [Space]
    [SerializeField] private PlayOneShotAndDestroy pref_PlayOneShot;
    [SerializeField] private MusicController pref_Music;

    private void OnEnable()
    {
        playerController = FindObjectOfType<PlayerController>();
        music_gameObject = FindObjectOfType<MusicController>();
        if (music_gameObject == null) music_gameObject = Instantiate(pref_Music);
        WarningResetLevel_Text.gameObject.SetActive(false);
        WarningOpenDoor_Text.gameObject.SetActive(false);
        Timer = TimeReload;
    }
    private void Update()
    {
        if (Pause) return;
        Timer_Text.text = Timer.ToString("F0");
        if (Timer > 0) Timer -= Time.deltaTime;
        else RestartLevel();
    }
    public void Warning_ShowAndRestart(PlayerController player)
    {
        WarningResetLevel_Text.gameObject.SetActive(true);
        StartCoroutine(RestartLevelWithDelay(2));
        Destroy(player.gameObject);
    }
    private IEnumerator RestartLevelWithDelay(float time)
    {
        yield return new WaitForSeconds(time);
        RestartLevel();
    }
    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void GiveEffectPlayer(Effect effect)
    {
        playerController.AddEffectToStorage(effect);
    }
    public void Warning_OpenDoor()
    {
        WarningOpenDoor_Text.gameObject.SetActive(true);
    }
    public void PlayOneShotAndDestroy(AudioClip audioClip, Transform source)
    {
        PlayOneShotAndDestroy audioSource = Instantiate(pref_PlayOneShot, source.position, source.rotation);
        audioSource.Play(audioClip);
    }
    public void FinishLevel(string LevelName)
    {
        PlayOneShotAndDestroy(FinishLevel_AudioClip, transform);
        playerController.FinishLevel();
        StartCoroutine(NextLevel(2, LevelName));
        Pause = true;
    }
    private IEnumerator NextLevel(float time, string LevelName)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(LevelName);
    }
}