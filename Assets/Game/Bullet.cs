using UnityEngine;
using Enemy;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class Bullet : MonoBehaviour
{
    [SerializeField] public GameObject Owner;
    [SerializeField] public Rigidbody2D body;
    [SerializeField] public float TimeLife = 2f;
    [SerializeField] private AudioClip Destroy_AudioClip;
    private void OnEnable()
    {
        body = GetComponent<Rigidbody2D>();
        Destroy(gameObject, TimeLife);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.transform.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<BaseEnemyController>().TakeDamage(Owner);
        }
        else
        {
            //GetComponent<AudioSource>().PlayOneShot(Destroy_AudioClip);
        }
        Destroy(gameObject);
    }
}