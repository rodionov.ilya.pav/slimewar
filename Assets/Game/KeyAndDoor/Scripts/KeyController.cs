using UnityEngine;
using Player;

public class KeyController : MonoBehaviour
{
    [SerializeField] private Effect effect;
    [SerializeField] public AudioClip PickUp_AudioClip;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerController player = collision.GetComponent<PlayerController>();
            if (player != null)
            {
                player.gameController.PlayOneShotAndDestroy(PickUp_AudioClip, transform);
                player.gameController.GiveEffectPlayer(effect);
                Destroy(gameObject);
            }
        }
    }
}