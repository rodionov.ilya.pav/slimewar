using UnityEngine;
using Player;
[CreateAssetMenu(fileName = "New Effect", menuName = "Effects/Effect_PowerUpJump", order = 51)]

public class Effect_PowerUpJump : Effect
{
    public override void Execute(PlayerController player)
    {
        player.PowerUpJump(Power);
    }
}