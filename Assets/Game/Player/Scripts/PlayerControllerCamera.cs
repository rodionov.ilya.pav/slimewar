using UnityEngine;

namespace Player
{
    public class PlayerControllerCamera : MonoBehaviour
    {
        private PlayerController playerController;
        private void OnEnable()
        {
            playerController = GetComponent<PlayerController>();
        }
        private void Update()
        {
            playerController.gameController.transform.position = new Vector3(transform.position.x + 5, transform.position.y, playerController.gameController.transform.position.z);
            if (playerController.gameController.transform.localPosition.y <= 0)
            {
                playerController.gameController.transform.position = new Vector3(transform.position.x + 5, 0, playerController.gameController.transform.position.z);
            }
        }
    }
}