using Unity.VisualScripting;
using UnityEngine;
namespace Player
{
    public class PlayerControllerAttack : MonoBehaviour
    {
        [SerializeField] private MouseButton mouseButton = MouseButton.Left;
        [SerializeField] private float SpeedBullet;
        [SerializeField] private Bullet pref_Bullet;
        private PlayerController playerController;
        private void OnEnable()
        {
            playerController = GetComponent<PlayerController>();
        }
        private void Update()
        {
            if (Input.GetMouseButtonDown((int)mouseButton))
            {
                Bullet bullet = Instantiate(pref_Bullet, playerController.ShootTransform.position, Quaternion.identity);
                bullet.body.AddForce(SpeedBullet * playerController.ShootTransform.TransformDirection(playerController.ShootTransform.right));
                GetComponent<AudioSource>().PlayOneShot(playerController.Shoot_AudioClip);
                bullet.Owner = gameObject;
            }
        }
    }
}