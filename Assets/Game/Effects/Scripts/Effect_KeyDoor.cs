using Player;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "Effects/Key", order = 51)]

public class Effect_KeyDoor : Effect
{
    public override void Execute(PlayerController player)
    {
        player.IsKey = true;
    }
}