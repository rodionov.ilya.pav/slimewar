using UnityEngine.UI;
using UnityEngine;
namespace Player
{
    public class PlayerControllerEffects : MonoBehaviour
    {
        [SerializeField] private int CountActiveEffects = 0;
        [SerializeField] private Image pref_ImageEffect;
        private PlayerController playerController;
        private void OnEnable()
        {
            playerController = GetComponent<PlayerController>();
        }
        private void Start()
        {
            GetEffectsFromStorage();
        }
        public void AddEffectToStorage(Effect effect)
        {
            DataBase.StorageEffects.Add(effect);
            SetEffectFromPlayer(effect);
            Debug.Log("PlayerControllerEffects: AddEffectToStorage - " + effect.name);
            Debug.Log("PlayerControllerEffects: AddEffectToStorage - " + DataBase.StorageEffects.Count);
        }
        public void GetEffectsFromStorage()
        {
            for (int i = 0; i < DataBase.StorageEffects.Count; i++)
            {
                SetEffectFromPlayer(DataBase.StorageEffects[i]);

                Debug.Log("PlayerControllerEffects: GetEffectsFromStorage - " + DataBase.StorageEffects[i].name);
            }
            ClearEffectToStorage();
            Debug.Log("PlayerControllerEffects: GetEffectsFromStorage");
        }
        public void SetEffectFromPlayer(Effect effect)
        {
            effect.Execute(playerController);
            Image image = Instantiate(pref_ImageEffect, playerController.gameController.PanelEffects.transform, false);
            image.rectTransform.anchorMin = new Vector2(0, 0.5f);
            image.rectTransform.anchorMax = new Vector2(0, 0.5f);
            image.rectTransform.anchoredPosition = new Vector3(50 + 120 * CountActiveEffects, 0);
            image.sprite = effect.icon;
            CountActiveEffects++;
        }
        public void ClearEffectToStorage()
        {
            DataBase.StorageEffects.Clear();
            Debug.Log("PlayerControllerEffects: ClearEffectToStorage");
        }
    }
}