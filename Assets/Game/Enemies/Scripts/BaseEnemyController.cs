using UnityEngine;
using Player;
namespace Enemy
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class BaseEnemyController : MonoBehaviour
    {
        [SerializeField] private float Health = 1;
        [SerializeField] private Effect effect;
        [SerializeField] private GameController gameController;
        [SerializeField] private AudioClip Dead_AudioClip;
        private void OnEnable()
        {
            gameController = FindObjectOfType<GameController>();
            if (gameController == null)
            {
                Debug.LogError("GameController Not Found!");
                enabled = false;
                return;
            }
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.CompareTag("Player"))
            {
                collision.gameObject.GetComponent<PlayerController>().TakeDamage();
            }

        }
        public void TakeDamage(GameObject other, float DamageCount = 1)
        {
            Health -= DamageCount;
            if (Health > 0) return;

            if (other != null && other.CompareTag("Player"))
            {
                gameController.GiveEffectPlayer(effect);
            }
            if (Dead_AudioClip != null) gameController.PlayOneShotAndDestroy(Dead_AudioClip, transform);
            Destroy(gameObject);
        }
    }
}