using UnityEngine.SceneManagement;
using UnityEngine;

public class BackMenu : MonoBehaviour
{
    public void BackMenuClick()
    {
        SceneManager.LoadScene("MainMenu");
    }
}